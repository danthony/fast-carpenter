# -*- coding: utf-8 -*-

"""Top-level package for fast-carpenter."""

__author__ = """Benjamin Krikler, and F.A.S.T"""
__email__ = 'fast-hep@cern.ch'
__version__ = '0.5.6'
